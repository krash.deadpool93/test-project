import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocsListComponent } from './components/docs-list/docs-list.component';
import { DocsViewComponent } from './components/docs-view/docs-view.component';

const routes: Routes = [
  {
    path: 'country',
    component: DocsViewComponent,
    // children: [{
    //     path: 'profile',
    //     component: DocsViewComponent,
    //     data: {breadcrumbs: 'Profile info'}, pathMatch: 'full'
    // }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
