import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ISome } from 'src/app/models/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.scss']
})
export class DocsListComponent implements OnInit {

  @Input()
  public items: ISome[] = [];
  public docs: ISome[] = [];

  constructor(
    private serviceData: DataService,
    private activRouter: ActivatedRoute,
    private router: Router
  ) { 
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.items.length ? this.docs = this.items : this.takeData();
  }

  public takeData(): void {
    this.serviceData.getAllDocs().subscribe((data) => {
      this.docs = data.data;
      this.docs = this.serviceData.setIsOpen(this.docs);
      this.activRouter.queryParams.subscribe((param) => {
        if (param['id']) {
          this.serviceData.itemFromQuery(this.docs, param['id']);
        };
       
      })
    })
  }

  public navigateAndToggle(item: ISome): void {
    item.isOpen = !item.isOpen;
    this.serviceData.selectedFile.next(item);
    this.router.navigate(['country'], {queryParams: {id: item.id}});
  }
}
