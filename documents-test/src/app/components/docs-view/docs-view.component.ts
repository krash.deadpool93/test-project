import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ISome } from 'src/app/models/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-docs-view',
  templateUrl: './docs-view.component.html',
  styleUrls: ['./docs-view.component.scss']
})
export class DocsViewComponent implements OnInit {

  public selectedItemId: string = '';
  public selectedItem!: ISome;
  public title: string = '';
  public typeFile: string = '';

  constructor(private activeRoute: ActivatedRoute, private serviceData: DataService) { }

  ngOnInit(): void {
    this.serviceData.selectedFile.subscribe(item => {
      this.selectedItem = item;
      this.title = this.selectedItem.title;

    })
    this.activeRoute.queryParams.subscribe((param) => {
      this.selectedItemId = param['id'];
    })
  }

  public selecTypeFile(): string {
    return this.selectedItem.isFolder ? `Показана папка: ${this.selectedItem.title}` : `Показан файл: ${this.selectedItem.title}`;
  }

}
