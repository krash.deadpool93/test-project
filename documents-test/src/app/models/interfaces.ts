export interface IData {
    data: ISome[]
}

export interface ISome {
    id: number,
    isFolder: boolean,
    title: string,
    items: ISome[],
    isOpen?: boolean
}