import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { IData, ISome } from '../models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public selectedFile = new BehaviorSubject<ISome>({items : [], title: '', id: NaN, isFolder: false});
  public selectedId = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) { }

  public getAllDocs(): Observable<IData> {
    let url = '../assets/documents.json';
    return this.http.get<any>(url);
  }

  public setIsOpen(docs: ISome[]): ISome[] {
   docs.forEach(doc => {
    doc.isOpen = false;
    if (doc.items.length) {
      this.setIsOpen(doc.items);
    }
   });
   return docs; 
  }

  public itemFromQuery(docs: ISome[], id: number): void {
    docs.forEach((doc) => {
      if (doc.id === id) {
        doc.isOpen = true;
        this.selectedFile.next(doc);
      } else {
        this.itemFromQuery(doc.items, id);
      }
    })
  }


}
